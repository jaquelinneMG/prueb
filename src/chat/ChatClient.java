package chat;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
public class ChatClient extends Thread{
    private Socket SocketCliente;
    private DataInputStream entrada;
    private PanelChat cliente;
    private ObjectInputStream entradaObjeto;
    
    public ChatClient(Socket SocketCliente, PanelChat cliente) { 
        this.SocketCliente = SocketCliente;
        this.cliente = cliente;
    }
    
    public void run() {
        while(true) {
            try {
                entrada =  new DataInputStream(SocketCliente.getInputStream());
                cliente.mensajeria(entrada.readUTF());
                entradaObjeto = new ObjectInputStream(SocketCliente.getInputStream());
                cliente.actualizarLista((DefaultListModel) entradaObjeto.readObject());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}