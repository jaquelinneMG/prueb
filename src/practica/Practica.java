package practica;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


public class Practica implements ActionListener {
    
    JFrame ventana;
    JTextField Nombre;
    JLabel ibNombre;
    JButton btAccion;
    
    public static void main(String[] args) {
        Practica app=new Practica();
        app.run();
    }
    void run(){
        ventana =new JFrame("Practica1");
        ventana.setSize(300, 200);
        ventana.setLayout(new FlowLayout());
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        ibNombre=new JLabel("Escribir nombre ");
        Nombre =new JTextField(10);
        btAccion=new JButton("seguir");
        
        btAccion.addActionListener((ActionEvent e)->{
            JOptionPane.showMessageDialog(ventana,"hola  "+Nombre.getText());
        });
        ventana.add(ibNombre);
        ventana.add(Nombre);
        ventana.add(btAccion);
        ventana.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
       JOptionPane.showInternalMessageDialog(ventana,"hola  "+this.Nombre.getText());
    }
}
